/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import com.intellij.idea.plugin.hybris.project.descriptors.HybrisModuleDescriptorType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.model.JpsElementChildRole;
import org.jetbrains.jps.model.ex.JpsCompositeElementBase;
import org.jetbrains.jps.model.ex.JpsElementChildRoleBase;
import org.jetbrains.jps.model.module.JpsModule;

/**
 * @author Eugene.Kudelevsky
 */
public class JpsHybrisModuleExtension extends JpsCompositeElementBase<JpsHybrisModuleExtension> {

    private static final JpsElementChildRole<JpsHybrisModuleExtension> ROLE = JpsElementChildRoleBase.create("Hybris");

    private HybrisModuleDescriptorType descriptorType;

    private JpsHybrisModuleExtension() {
    }

    private JpsHybrisModuleExtension(final HybrisModuleDescriptorType descriptorType) {
        this.descriptorType = descriptorType;
    }

    @NotNull
    @Override
    public JpsHybrisModuleExtension createCopy() {
        return new JpsHybrisModuleExtension(descriptorType);
    }

    public void setDescriptorType(final HybrisModuleDescriptorType descriptorType) {
        this.descriptorType = descriptorType;
    }

    public HybrisModuleDescriptorType getDescriptorType() {
        return descriptorType;
    }

    @Nullable
    public static JpsHybrisModuleExtension getExtension(@Nullable final JpsModule module) {
        return module != null ? module.getContainer().getChild(ROLE) : null;
    }

    @NotNull
    public static JpsHybrisModuleExtension getOrCreateExtension(@NotNull final JpsModule module) {
        JpsHybrisModuleExtension extension = getExtension(module);

        if (extension == null) {
            extension = module.getContainer().setChild(
                ROLE,
                new JpsHybrisModuleExtension()
            );
        }
        return extension;
    }
}

