/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import com.intellij.openapi.util.io.FileUtilRt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.builders.BuildRootDescriptor;
import org.jetbrains.jps.builders.BuildTarget;

import java.io.File;

/**
 * @author Eugene.Kudelevsky
 */
public class AddOnResourcesRootDescriptor extends BuildRootDescriptor {

    @NotNull
    private final AddOnResourcesTarget target;
    @NotNull
    private final String id;
    @NotNull
    private final File root;
    @NotNull
    private final String addOnName;

    private final boolean resourceRoot;

    public AddOnResourcesRootDescriptor(
        @NotNull final AddOnResourcesTarget target,
        @NotNull final File root,
        @NotNull final String addOnName,
        final boolean resourceRoot
    ) {
        this.target = target;
        this.root = root;
        this.id = FileUtilRt.toSystemIndependentName(root.getAbsolutePath()) + '_' + resourceRoot;
        this.addOnName = addOnName;
        this.resourceRoot = resourceRoot;
    }

    @Override
    public String getRootId() {
        return id;
    }

    @Override
    public File getRootFile() {
        return root;
    }

    @Override
    public BuildTarget<?> getTarget() {
        return target;
    }

    @NotNull
    public String getAddOnName() {
        return addOnName;
    }

    public boolean isResourceRoot() {
        return resourceRoot;
    }
}
