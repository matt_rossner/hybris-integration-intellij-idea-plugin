/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import com.intellij.idea.plugin.hybris.project.descriptors.HybrisModuleDescriptorType;
import com.intellij.openapi.vfs.VfsUtilCore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.builders.BuildTarget;
import org.jetbrains.jps.builders.BuildTargetRegistry;
import org.jetbrains.jps.builders.TargetOutputIndex;
import org.jetbrains.jps.builders.storage.BuildDataPaths;
import org.jetbrains.jps.incremental.CompileContext;
import org.jetbrains.jps.incremental.ModuleBuildTarget;
import org.jetbrains.jps.indices.IgnoredFileIndex;
import org.jetbrains.jps.indices.ModuleExcludeIndex;
import org.jetbrains.jps.model.JpsModel;
import org.jetbrains.jps.model.module.JpsModule;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.intellij.idea.plugin.hybris.common.HybrisConstants.BACK_OFFICE_MODULE_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.BIN_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.CLASSES_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.HMC_MODULE_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.RESOURCES_DIRECTORY;

/**
 * @author Eugene.Kudelevsky
 */
public class JarsGeneratingTarget extends ModuleBasedTargetBase<JarsGeneratingRootDescriptor> {

    public JarsGeneratingTarget(@NotNull final JpsModule module) {
        super(MyTargetType.INSTANCE, module);
    }

    @NotNull
    @Override
    public List<JarsGeneratingRootDescriptor> computeRootDescriptors(
        final JpsModel model,
        final ModuleExcludeIndex index,
        final IgnoredFileIndex ignoredFileIndex,
        final BuildDataPaths dataPaths
    ) {
        return myModule
            .getContentRootsList().getUrls().stream()
            .flatMap(url -> {
                final String parent = VfsUtilCore.urlToPath(url);
                return Stream.of(createBackofficeRootDescriptor(parent), createHmcRootDescriptor(parent));
            })
            .collect(Collectors.toList());
    }

    @NotNull
    private JarsGeneratingRootDescriptor createBackofficeRootDescriptor(@NotNull final String parent) {
        final String extensionName = myModule.getName();
        final File srcDir = new File(parent, BACK_OFFICE_MODULE_DIRECTORY + '/' + CLASSES_DIRECTORY);
        final File outputJar = new File(
            parent,
            RESOURCES_DIRECTORY + '/' + BACK_OFFICE_MODULE_DIRECTORY + '/' + extensionName + "_bof.jar"
        );
        return new JarsGeneratingRootDescriptor(this, srcDir, outputJar);
    }

    @NotNull
    private JarsGeneratingRootDescriptor createHmcRootDescriptor(@NotNull final String parent) {
        final String extensionName = myModule.getName();
        final File srcDir = new File(parent, HMC_MODULE_DIRECTORY + '/' + CLASSES_DIRECTORY);
        final File outputJar = new File(
            parent,
            HMC_MODULE_DIRECTORY + '/' + BIN_DIRECTORY + '/' + extensionName + "hmc.jar"
        );
        return new JarsGeneratingRootDescriptor(this, srcDir, outputJar);
    }

    @NotNull
    @Override
    public Collection<File> getOutputRoots(final CompileContext context) {
        // We don't return actual output folders, since they cannot contain additional content,
        // because they can be cleared before compilation
        return Collections.emptyList();
    }

    @Override
    public Collection<BuildTarget<?>> computeDependencies(
        final BuildTargetRegistry targetRegistry, final TargetOutputIndex outputIndex
    ) {
        return targetRegistry
            .getModuleBasedTargets(myModule, BuildTargetRegistry.ModuleTargetSelector.ALL).stream()
            .filter(target -> target instanceof ModuleBuildTarget)
            .collect(Collectors.toList());
    }

    public static class MyTargetType extends ModuleBasedBuildTargetTypeBase<JarsGeneratingTarget> {

        public static final MyTargetType INSTANCE = new MyTargetType();

        private MyTargetType() {
            super("hybris-jars-generating");
        }

        @Override
        protected boolean isAcceptableModule(@NotNull final JpsModule module) {
            final JpsHybrisModuleExtension extension = JpsHybrisModuleExtension.getExtension(module);

            if (extension == null) {
                return false;
            }
            final HybrisModuleDescriptorType type = extension.getDescriptorType();
            return type != HybrisModuleDescriptorType.OOTB && type != HybrisModuleDescriptorType.PLATFORM;
        }

        @NotNull
        @Override
        protected JarsGeneratingTarget createTarget(@NotNull final JpsModule module) {
            return new JarsGeneratingTarget(module);
        }
    }
}
