/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import com.intellij.idea.plugin.hybris.common.HybrisConstants;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VfsUtilCore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.builders.storage.BuildDataPaths;
import org.jetbrains.jps.incremental.CompileContext;
import org.jetbrains.jps.indices.IgnoredFileIndex;
import org.jetbrains.jps.indices.ModuleExcludeIndex;
import org.jetbrains.jps.model.JpsModel;
import org.jetbrains.jps.model.java.JpsJavaExtensionService;
import org.jetbrains.jps.model.module.JpsModule;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Eugene.Kudelevsky
 */
public class AddOnResourcesTarget extends ModuleBasedTargetBase<AddOnResourcesRootDescriptor> {

    public AddOnResourcesTarget(@NotNull final JpsModule module) {
        super(MyTargetType.INSTANCE, module);
    }

    @NotNull
    @Override
    public List<AddOnResourcesRootDescriptor> computeRootDescriptors(
        final JpsModel model,
        final ModuleExcludeIndex index,
        final IgnoredFileIndex ignoredFileIndex,
        final BuildDataPaths dataPaths
    ) {
        // We don't check if add-on folders exist, because root descriptors have to depend on a configuration only
        return JpsJavaExtensionService
            .dependencies(myModule)
            .productionOnly()
            .recursivelyExportedOnly()
            .getModules().stream()
            .filter(depModule -> !depModule.equals(myModule))
            .flatMap(depModule -> depModule
                .getContentRootsList().getUrls().stream()
                .map(url -> new File(VfsUtilCore.urlToPath(url), HybrisConstants.ACCELERATOR_ADDON_DIRECTORY))
                .flatMap(addOnDir -> Stream.concat(Stream.of(
                    new AddOnResourcesRootDescriptor(
                        this,
                        new File(addOnDir, HybrisConstants.WEB_ROOT_DIRECTORY_RELATIVE_PATH),
                        depModule.getName(),
                        true
                    )), getSourceDescriptorStream(depModule, addOnDir))))
            .collect(Collectors.toList());
    }

    @NotNull
    private Stream<AddOnResourcesRootDescriptor> getSourceDescriptorStream(
        @NotNull final JpsModule module,
        @NotNull final File addOnDir
    ) {
        return module.getSourceRoots().stream()
            .filter(sourceRoot -> FileUtil.isAncestor(addOnDir, sourceRoot.getFile(), false))
            .map(sourceRoot -> new AddOnResourcesRootDescriptor(
                this,
                sourceRoot.getFile(),
                module.getName(),
                false
            ));
    }

    @NotNull
    @Override
    public Collection<File> getOutputRoots(final CompileContext context) {
        // We don't return actual output folders, because they:
        // 1. Cannot contain additional content, because they can be cleared before compilation
        // 2. Has to depend on the configuration only, cannot depend on current FS state
        // Actual output folders look like "<some-path-inside-web-root>/addons/<add-on-name>"
        return Collections.emptyList();
    }

    @Override
    public boolean isCompiledBeforeModuleLevelBuilders() {
        return true;
    }

    public static class MyTargetType extends ModuleBasedBuildTargetTypeBase<AddOnResourcesTarget> {

        public static final MyTargetType INSTANCE = new MyTargetType();

        private MyTargetType() {
            super("hybris-add-on-resources-copying");
        }

        @NotNull
        @Override
        protected AddOnResourcesTarget createTarget(@NotNull final JpsModule module) {
            return new AddOnResourcesTarget(module);
        }
    }
}
