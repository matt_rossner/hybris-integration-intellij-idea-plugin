/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.builder;

import com.intellij.idea.plugin.hybris.common.HybrisUtil;
import com.intellij.idea.plugin.hybris.jps.JpsUtil;
import com.intellij.idea.plugin.hybris.jps.model.JpsHybrisProjectExtension;
import com.intellij.idea.plugin.hybris.jps.model.JpsHybrisProjectSettingsState;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.util.io.FileUtilRt;
import com.intellij.util.containers.ContainerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.builders.storage.BuildDataPaths;
import org.jetbrains.jps.incremental.CompileContext;
import org.jetbrains.jps.incremental.java.ClassPostProcessor;
import org.jetbrains.jps.incremental.messages.BuildMessage;
import org.jetbrains.jps.incremental.messages.CompilerMessage;
import org.jetbrains.jps.javac.OutputFileObject;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.intellij.idea.plugin.hybris.common.HybrisConstants.BACK_OFFICE_MODULE_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.CLASSES_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.HMC_MODULE_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.TEST_CLASSES_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.TEST_SRC_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.WEB_INF_CLASSES_DIRECTORY;
import static com.intellij.idea.plugin.hybris.common.HybrisConstants.WEB_MODULE_DIRECTORY;

/**
 * @author Eugene.Kudelevsky
 */
public class JavaClassPostProcessor implements ClassPostProcessor {

    private static final Logger LOG = Logger.getInstance(JavaClassPostProcessor.class);
    private static final String BUILDER_NAME = "Hybris Classes Processor";

    @Override
    public void process(final CompileContext context, final OutputFileObject out) {
        final JpsHybrisProjectExtension extension = JpsHybrisProjectExtension.getExtension(
            context.getProjectDescriptor().getProject());
        final JpsHybrisProjectSettingsState state = extension == null ? null : extension.getState();

        if (state == null || !state.isHybrisProject()) {
            return;
        }
        final File sourceFile = out.getSourceFile();
        final String relativePath = out.getRelativePath();

        if (sourceFile == null || relativePath == null) {
            return;
        }
        final Collection<File> antOutputRoots = ContainerUtil.subtract(
            computeAntOutputRoots(context, out),
            Collections.singletonList(out.getOutputRoot())
        );
        final File fromFile = out.getFile();

        for (File antOutputRoot : antOutputRoots) {
            // Perform copying to directories that were created by Ant. See the comment by Alex B. in IIP-286 for details.
            if (antOutputRoot.exists()) {
                final File toFile = new File(antOutputRoot, relativePath);

                try {
                    FileUtilRt.copy(fromFile, toFile);
                } catch (IOException e) {
                    final String fromFilePath = FileUtilRt.toSystemDependentName(fromFile.getPath());
                    final String toFilePath = FileUtilRt.toSystemDependentName(toFile.getPath());

                    context.processMessage(new CompilerMessage(
                        BUILDER_NAME,
                        BuildMessage.Kind.ERROR,
                        "Cannot copy file '" + fromFilePath + "' to '" + toFilePath + '\''
                    ));
                    LOG.info(e);
                }
            }
        }
    }

    @NotNull
    private static List<File> computeAntOutputRoots(
        @NotNull final CompileContext context,
        @NotNull final OutputFileObject fileObject
    ) {
        final File sourceRoot = computeSourceRoot(fileObject);

        if (sourceRoot == null) {
            return Collections.emptyList();
        }
        final File hybrisRootForGen = computeHybrisRootForGeneratedSourceFile(context, sourceRoot);

        if (hybrisRootForGen != null) {
            // we generate sources for web add-ons only, so return .../WEB-INF/classes dir
            return Collections.singletonList(new File(hybrisRootForGen, WEB_INF_CLASSES_DIRECTORY));
        }
        final File hybrisRoot = computeHybrisRoot(sourceRoot);

        if (hybrisRoot == null) {
            return Collections.emptyList();
        }
        final boolean test = TEST_SRC_DIRECTORY.equals(sourceRoot.getName());
        final File sourceRootParent = sourceRoot.getParentFile();

        if (new File(hybrisRoot, WEB_MODULE_DIRECTORY).equals(sourceRootParent)) {
            final String dirName = test
                ? WEB_MODULE_DIRECTORY + '/' + TEST_CLASSES_DIRECTORY
                : WEB_INF_CLASSES_DIRECTORY;
            return Collections.singletonList(new File(hybrisRoot, dirName));
        }
        // Copy tests both to "classes" and "testclasses" to envisage possible changes of ANT scripts
        final List<String> allDirNames = test
            ? Arrays.asList(CLASSES_DIRECTORY, TEST_CLASSES_DIRECTORY)
            : Collections.singletonList(CLASSES_DIRECTORY);

        if (new File(hybrisRoot, HMC_MODULE_DIRECTORY).equals(sourceRootParent)) {
            return allDirNames
                .stream()
                .map(dirName -> new File(hybrisRoot, HMC_MODULE_DIRECTORY + '/' + dirName))
                .collect(Collectors.toList());
        }
        if (new File(hybrisRoot, BACK_OFFICE_MODULE_DIRECTORY).equals(sourceRootParent)) {
            return allDirNames
                .stream()
                .map(dirName -> new File(hybrisRoot, BACK_OFFICE_MODULE_DIRECTORY + '/' + dirName))
                .collect(Collectors.toList());
        }
        return allDirNames
            .stream()
            .map(dirName -> new File(hybrisRoot, dirName))
            .collect(Collectors.toList());
    }

    @Nullable
    private static File computeHybrisRoot(@NotNull final File file) {
        File result = file;

        while (result != null && !HybrisUtil.isHybrisModuleRoot(result)) {
            result = result.getParentFile();
        }
        return result;
    }

    @Nullable
    private static File computeHybrisRootForGeneratedSourceFile(
        final @NotNull CompileContext context,
        final @NotNull File file
    ) {
        final BuildDataPaths dataPaths = context.getProjectDescriptor().dataManager.getDataPaths();

        return context.getProjectDescriptor().getProject().getModules()
                      .stream()
                      .filter(JpsUtil::isHybrisModule)
                      .filter(module -> {
                          final File genStorage = JpsUtil.getGeneratedSourcesStorage(module, dataPaths);
                          return FileUtil.isAncestor(genStorage, file, false);
                      })
                      .map(JpsUtil::findHybrisModuleRoot)
                      .findAny()
                      .orElse(null);
    }

    @Nullable
    private static File computeSourceRoot(@NotNull final OutputFileObject fileObject) {
        final File sourceFile = fileObject.getSourceFile();
        final File outputFile = fileObject.getFile();
        final File outputRoot = fileObject.getOutputRoot();

        if (sourceFile != null && outputRoot != null) {
            File result = sourceFile.getParentFile();
            File outDir = outputFile.getParentFile();

            while (result != null &&
                   outDir != null &&
                   result.getName().equals(outDir.getName()) &&
                   !outputRoot.equals(outDir)) {

                result = result.getParentFile();
                outDir = outDir.getParentFile();
            }
            if (outputRoot.equals(outDir)) {
                return result;
            }
        }
        return null;
    }
}
