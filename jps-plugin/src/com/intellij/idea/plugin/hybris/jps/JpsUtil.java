/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps;

import com.intellij.idea.plugin.hybris.common.HybrisConstants;
import com.intellij.idea.plugin.hybris.common.HybrisUtil;
import com.intellij.idea.plugin.hybris.jps.model.JpsHybrisModuleExtension;
import com.intellij.idea.plugin.hybris.jps.model.JpsHybrisProjectExtension;
import com.intellij.idea.plugin.hybris.jps.model.JpsHybrisProjectSettingsState;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VfsUtilCore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.builders.storage.BuildDataPaths;
import org.jetbrains.jps.incremental.CompileContext;
import org.jetbrains.jps.incremental.messages.BuildMessage.Kind;
import org.jetbrains.jps.incremental.messages.CompilerMessage;
import org.jetbrains.jps.model.module.JpsModule;

import java.io.File;

/**
 * @author Eugene.Kudelevsky
 */
public final class JpsUtil {

    private static final Logger LOG = Logger.getInstance(JpsUtil.class);
    private static final String STORAGE_DIR_NAME = "hybris";
    private static final String GENERATED_SOURCES_DIR_NAME = "generated_sources";
    public static final String ADD_ONS_DIR_NAME = "addons";

    private JpsUtil() {
    }

    public static boolean isHybrisModule(@NotNull final JpsModule module) {
        final JpsHybrisProjectExtension projectExtension = JpsHybrisProjectExtension.getExtension(module.getProject());
        final JpsHybrisProjectSettingsState projectState = projectExtension == null
            ? null
            : projectExtension.getState();

        return projectState != null && projectState.isHybrisProject() &&
               JpsHybrisModuleExtension.getExtension(module) != null;
    }

    @Nullable
    public static File findHybrisModuleRoot(@NotNull final JpsModule module) {
        for (String url : module.getContentRootsList().getUrls()) {
            final File file = new File(VfsUtilCore.urlToPath(url));

            if (HybrisUtil.isHybrisModuleRoot(file)) {
                return file;
            }
        }
        return null;
    }

    public static boolean containsAddOn(@NotNull final JpsModule module) {
        return findHybrisAddOnDirectory(module) != null;
    }

    @Nullable
    public static File findHybrisAddOnDirectory(@NotNull final JpsModule module) {
        if (!JpsUtil.isHybrisModule(module)) {
            return null;
        }
        final File hybrisModuleRoot = JpsUtil.findHybrisModuleRoot(module);

        if (hybrisModuleRoot == null) {
            return null;
        }
        final File addOnDirectory = new File(hybrisModuleRoot, HybrisConstants.ACCELERATOR_ADDON_DIRECTORY);
        return addOnDirectory.isDirectory() ? addOnDirectory : null;
    }

    @NotNull
    public static File getGeneratedSourcesStorage(
        @NotNull final JpsModule module,
        @NotNull final BuildDataPaths dataPaths
    ) {
        return new File(getStorage(module, dataPaths), GENERATED_SOURCES_DIR_NAME);
    }

    @NotNull
    private static File getStorage(final @NotNull JpsModule module, final @NotNull BuildDataPaths dataPaths) {
        return new File(new File(dataPaths.getDataStorageRoot(), STORAGE_DIR_NAME), module.getName());
    }

    public static void reportExceptionError(
        @NotNull final CompileContext context,
        @Nullable final String filePath,
        @Nullable final String message,
        @NotNull final Exception exception,
        @NotNull final String builderName
    ) {
        final String exceptionMessage = exception.getMessage();

        if (exceptionMessage != null) {
            final String fullMessage = StringUtil.isNotEmpty(message)
                ? message + ": " + exceptionMessage
                : exceptionMessage;

            context.processMessage(new CompilerMessage(builderName, Kind.ERROR, fullMessage, filePath));
        } else if (StringUtil.isEmpty(message)) {
            context.processMessage(new CompilerMessage(builderName, exception));
        } else {
            context.processMessage(new CompilerMessage(builderName, Kind.ERROR, message));
        }
        LOG.debug(exception);
    }

}
