/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.model.JpsElementChildRole;
import org.jetbrains.jps.model.JpsProject;
import org.jetbrains.jps.model.ex.JpsCompositeElementBase;
import org.jetbrains.jps.model.ex.JpsElementChildRoleBase;

/**
 * @author Eugene.Kudelevsky
 */
public class JpsHybrisProjectExtension extends JpsCompositeElementBase<JpsHybrisProjectExtension> {

    private static final JpsElementChildRole<JpsHybrisProjectExtension> ROLE =
        JpsElementChildRoleBase.create("Hybris");

    private JpsHybrisProjectSettingsState state;

    private JpsHybrisProjectExtension(final JpsHybrisProjectSettingsState state) {
        this.state = state;
    }

    @NotNull
    @Override
    public JpsHybrisProjectExtension createCopy() {
        return new JpsHybrisProjectExtension(new JpsHybrisProjectSettingsState(state));
    }

    public void setState(final JpsHybrisProjectSettingsState state) {
        this.state = state;
    }

    public JpsHybrisProjectSettingsState getState() {
        return state;
    }

    @Nullable
    public static JpsHybrisProjectExtension getExtension(@Nullable final JpsProject project) {
        return project != null ? project.getContainer().getChild(ROLE) : null;
    }

    @NotNull
    public static JpsHybrisProjectExtension getOrCreateExtension(@NotNull final JpsProject project) {
        JpsHybrisProjectExtension extension = getExtension(project);

        if (extension == null) {
            extension = project.getContainer().setChild(
                ROLE,
                new JpsHybrisProjectExtension(new JpsHybrisProjectSettingsState())
            );
        }
        return extension;
    }
}
