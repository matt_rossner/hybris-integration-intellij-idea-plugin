/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.builder;

import com.intellij.idea.plugin.hybris.jps.JpsUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.builders.AdditionalRootsProviderService;
import org.jetbrains.jps.builders.BuildTarget;
import org.jetbrains.jps.builders.java.JavaModuleBuildTargetType;
import org.jetbrains.jps.builders.java.JavaSourceRootDescriptor;
import org.jetbrains.jps.builders.storage.BuildDataPaths;
import org.jetbrains.jps.incremental.ModuleBuildTarget;
import org.jetbrains.jps.model.java.JpsJavaExtensionService;
import org.jetbrains.jps.model.module.JpsModule;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Eugene.Kudelevsky
 */
public class HybrisAdditionalRootProviderService extends AdditionalRootsProviderService<JavaSourceRootDescriptor> {

    public HybrisAdditionalRootProviderService() {
        super(Collections.singletonList(JavaModuleBuildTargetType.PRODUCTION));
    }

    @NotNull
    @Override
    public List<JavaSourceRootDescriptor> getAdditionalRoots(
        @NotNull final BuildTarget<JavaSourceRootDescriptor> target, final BuildDataPaths dataPaths
    ) {
        if (!(target instanceof ModuleBuildTarget)) {
            return Collections.emptyList();
        }
        final ModuleBuildTarget moduleBuildTarget = (ModuleBuildTarget) target;
        final JpsModule module = moduleBuildTarget.getModule();

        if (!JpsUtil.isHybrisModule(module)) {
            return Collections.emptyList();
        }
        final File generatedSourcesStorage = JpsUtil.getGeneratedSourcesStorage(module, dataPaths);
        final File addOnsGeneratedSourceRoot = new File(generatedSourcesStorage, JpsUtil.ADD_ONS_DIR_NAME);

        return JpsJavaExtensionService
            .dependencies(module)
            .productionOnly()
            .recursivelyExportedOnly()
            .getModules().stream()
            .filter(depModule -> !depModule.equals(module))
            .filter(JpsUtil::containsAddOn)
            .map(depModule -> new File(addOnsGeneratedSourceRoot, depModule.getName()))
            .map(sourceRoot -> new JavaSourceRootDescriptor(
                sourceRoot,
                moduleBuildTarget,
                true,
                false,
                "",
                Collections.emptySet()
            ))
            .collect(Collectors.toList());
    }
}
