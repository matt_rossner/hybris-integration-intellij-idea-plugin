/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.builders.BuildRootDescriptor;
import org.jetbrains.jps.builders.BuildRootIndex;
import org.jetbrains.jps.builders.BuildTarget;
import org.jetbrains.jps.builders.BuildTargetRegistry;
import org.jetbrains.jps.builders.ModuleBasedBuildTargetType;
import org.jetbrains.jps.builders.ModuleBasedTarget;
import org.jetbrains.jps.builders.TargetOutputIndex;
import org.jetbrains.jps.model.module.JpsModule;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Eugene.Kudelevsky
 */
public abstract class ModuleBasedTargetBase<T extends BuildRootDescriptor> extends ModuleBasedTarget<T> {

    public ModuleBasedTargetBase(
        final ModuleBasedBuildTargetType targetType,
        @NotNull final JpsModule module
    ) {
        super(targetType, module);
    }

    @Nullable
    @Override
    public T findRootDescriptor(final String rootId, final BuildRootIndex rootIndex) {
        return rootIndex
            .getTargetRoots(this, null).stream()
            .filter(descriptor -> descriptor.getRootId().equals(rootId))
            .findFirst()
            .orElse(null);
    }

    @NotNull
    @Override
    public String getPresentableName() {
        return getTargetType().getTypeId() + ':' + myModule.getName();
    }

    @Override
    public boolean isTests() {
        return false;
    }

    @Override
    public String getId() {
        return myModule.getName();
    }

    @Override
    public Collection<BuildTarget<?>> computeDependencies(
        final BuildTargetRegistry targetRegistry,
        final TargetOutputIndex outputIndex
    ) {
        return Collections.emptyList();
    }
}
