/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.jps.builder;

import com.intellij.idea.plugin.hybris.jps.JpsUtil;
import com.intellij.idea.plugin.hybris.jps.model.JarsGeneratingRootDescriptor;
import com.intellij.idea.plugin.hybris.jps.model.JarsGeneratingTarget;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.io.ZipUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.jps.builders.BuildOutputConsumer;
import org.jetbrains.jps.builders.DirtyFilesHolder;
import org.jetbrains.jps.incremental.CompileContext;
import org.jetbrains.jps.incremental.ProjectBuildException;
import org.jetbrains.jps.incremental.StopBuildException;
import org.jetbrains.jps.incremental.TargetBuilder;
import org.jetbrains.jps.incremental.messages.BuildMessage.Kind;
import org.jetbrains.jps.incremental.messages.CompilerMessage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.jar.JarOutputStream;

/**
 * @author Eugene.Kudelevsky
 */
public class JarsGeneratingBuilder extends TargetBuilder<JarsGeneratingRootDescriptor, JarsGeneratingTarget> {

    private static final String BUILDER_NAME = "Hybris JARs Generating Compiler";

    protected JarsGeneratingBuilder() {
        super(Collections.singletonList(JarsGeneratingTarget.MyTargetType.INSTANCE));
    }

    @Override
    public void build(
        @NotNull final JarsGeneratingTarget target,
        @NotNull final DirtyFilesHolder<JarsGeneratingRootDescriptor, JarsGeneratingTarget> holder,
        @NotNull final BuildOutputConsumer outputConsumer,
        @NotNull final CompileContext context
    ) throws ProjectBuildException, IOException {
        if (!holder.hasDirtyFiles() && !holder.hasRemovedFiles()) {
            return;
        }
        if (!doBuild(holder, outputConsumer, context)) {
            //noinspection NewExceptionWithoutArguments
            throw new StopBuildException();
        }
    }

    private boolean doBuild(
        @NotNull final DirtyFilesHolder<JarsGeneratingRootDescriptor, JarsGeneratingTarget> holder,
        @NotNull final BuildOutputConsumer outputConsumer,
        @NotNull final CompileContext context
    ) throws IOException {
        final Set<JarsGeneratingRootDescriptor> dirtyRoots = ContainerUtil.newHashSet();

        holder.processDirtyFiles((target1, file, root) -> {
            dirtyRoots.add(root);
            return true;
        });
        for (JarsGeneratingRootDescriptor dirtyRoot : dirtyRoots) {
            final Set<String> srcFiles = zipDirectoryContent(
                dirtyRoot.getRootFile(),
                dirtyRoot.getOutputJar(),
                context
            );
            if (srcFiles == null) {
                return false;
            }
            if (!srcFiles.isEmpty()) {
                outputConsumer.registerOutputFile(dirtyRoot.getOutputJar(), srcFiles);
            }
        }
        return true;
    }

    @Nullable
    private static Set<String> zipDirectoryContent(
        @NotNull final File srcDir,
        @NotNull final File dstJar,
        @NotNull final CompileContext context
    ) throws IOException {
        if (dstJar.exists() && !FileUtil.delete(dstJar)) {
            context.processMessage(new CompilerMessage(
                BUILDER_NAME,
                Kind.ERROR,
                "Cannot delete file: " + dstJar.getAbsolutePath()
            ));
            return null;
        }
        final File parentDir = dstJar.getParentFile();

        if (!parentDir.exists() && !parentDir.mkdirs()) {
            context.processMessage(new CompilerMessage(
                BUILDER_NAME,
                Kind.ERROR,
                "Cannot create directory: " + parentDir.getAbsolutePath()
            ));
            return null;
        }
        return doZipDirectoryContent(srcDir, dstJar, context);
    }

    @Nullable
    private static Set<String> doZipDirectoryContent(
        final @NotNull File srcDir,
        final @NotNull File dstJar,
        @NotNull final CompileContext context
    ) throws IOException {
        final Set<String> srcFiles = ContainerUtil.newHashSet();
        final boolean success;

        try (JarOutputStream jos = new JarOutputStream(new FileOutputStream(dstJar))) {
            success = FileUtil.processFilesRecursively(srcDir, file -> {
                String relPath = FileUtil.getRelativePath(srcDir, file);

                if (relPath != null && file.isFile()) {
                    relPath = FileUtil.toSystemIndependentName(relPath);
                    final String filePath = file.getAbsolutePath();
                    final String errorMessage = "Cannot add file " + filePath + " to ZIP";
                    try {
                        if (!ZipUtil.addFileToZip(jos, file, relPath, null, null)) {
                            context.processMessage(new CompilerMessage(BUILDER_NAME, Kind.ERROR, errorMessage));
                            return false;
                        }
                        srcFiles.add(filePath);
                    } catch (IOException e) {
                        JpsUtil.reportExceptionError(context, null, errorMessage, e, BUILDER_NAME);
                        return false;
                    }
                }
                return true;
            });
        }
        return success ? srcFiles : null;
    }

    @NotNull
    @Override
    public String getPresentableName() {
        return BUILDER_NAME;
    }
}
