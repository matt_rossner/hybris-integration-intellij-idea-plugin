/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationTypeUtil;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.junit.JUnitConfigurationType;
import com.intellij.icons.AllIcons;
import com.intellij.idea.plugin.hybris.common.utils.HybrisIcons;
import com.intellij.openapi.project.Project;
import com.intellij.ui.LayeredIcon;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

import static com.intellij.idea.plugin.hybris.common.utils.HybrisI18NBundleUtils.message;

/**
 * @author Eugene.Kudelevsky
 */
public class HybrisTestConfigurationType extends JUnitConfigurationType {

    private static final LayeredIcon ICON;

    private final ConfigurationFactory[] configurationFactories =
        new ConfigurationFactory[]{new MyConfigurationFactory()};

    static {
        final LayeredIcon icon = new LayeredIcon(2);
        icon.setIcon(HybrisIcons.HYBRIS_ICON, 0);
        icon.setIcon(AllIcons.Nodes.JunitTestMark, 1);
        ICON = icon;
    }

    @Override
    public String getDisplayName() {
        return message("hybris.tests.configuration.type.name");
    }

    @Override
    public String getConfigurationTypeDescription() {
        return message("hybris.tests.configuration.type.description");
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }

    @Override
    public ConfigurationFactory[] getConfigurationFactories() {
        return configurationFactories;
    }

    @NotNull
    @Override
    public String getId() {
        return "HybrisTests";
    }

    @SuppressWarnings("MethodOverridesStaticMethodOfSuperclass")
    @NotNull
    public static HybrisTestConfigurationType getInstance() {
        return ConfigurationTypeUtil.findConfigurationType(HybrisTestConfigurationType.class);
    }

    private class MyConfigurationFactory extends ConfigurationFactory {

        protected MyConfigurationFactory() {
            super(HybrisTestConfigurationType.this);
        }

        @NotNull
        @Override
        public RunConfiguration createTemplateConfiguration(@NotNull final Project project) {
            return new HybrisTestConfiguration("", project, this);
        }
    }
}
