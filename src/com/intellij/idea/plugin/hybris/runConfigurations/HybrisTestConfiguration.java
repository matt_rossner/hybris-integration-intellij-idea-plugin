/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.diagnostic.logging.LogConfigurationPanel;
import com.intellij.execution.ExecutionBundle;
import com.intellij.execution.ExecutionException;
import com.intellij.execution.Executor;
import com.intellij.execution.JavaRunConfigurationExtensionManager;
import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.configurations.RuntimeConfigurationException;
import com.intellij.execution.executors.DefaultRunExecutor;
import com.intellij.execution.junit.JUnitConfiguration;
import com.intellij.execution.junit.TestObject;
import com.intellij.execution.junit2.configuration.JUnitConfigurable;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.runners.ExecutionEnvironmentBuilder;
import com.intellij.execution.testframework.TestSearchScope;
import com.intellij.execution.testframework.sm.runner.SMTRunnerConsoleProperties;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.options.SettingsEditorGroup;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.openapi.util.WriteExternalException;
import com.intellij.util.xml.NamedEnumUtil;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.intellij.idea.plugin.hybris.common.utils.HybrisI18NBundleUtils.message;

/**
 * @author Eugene.Kudelevsky
 */
public class HybrisTestConfiguration extends JUnitConfiguration {

    private static final String HYBRIS_TAG = "hybris";
    private static final String FILTER_ATTRIBUTE = "filter";

    @Nullable
    private ClassAnnotationTestFilter classAnnotationFilter;
    @Nullable
    private ScopeTestFilter scopeTestFilter = ScopeTestFilter.ALL_CUSTOM_MODULES;
    @Nullable
    private Module filteringModule;

    public HybrisTestConfiguration(
        final String name,
        final Project project,
        final ConfigurationFactory configurationFactory
    ) {
        super(name, project, new MyData(), configurationFactory);
        setModuleName(LocalExtensionsModuleGenerator.LOCAL_EXTENSIONS_MODULE_NAME);
        final JUnitConfiguration.Data data = getPersistentData();
        data.TEST_OBJECT = JUnitConfiguration.TEST_PACKAGE;
        data.PACKAGE_NAME = "";
        data.setScope(TestSearchScope.MODULE_WITH_DEPENDENCIES);
    }

    @NotNull
    @Override
    public SettingsEditor<? extends RunConfiguration> getConfigurationEditor() {
        final SettingsEditorGroup<HybrisTestConfiguration> group = new SettingsEditorGroup<>();
        group.addEditor(message("hybris.tests.hybris.tab"), new HybrisTestSettingsEditor());
        group.addEditor(message("hybris.tests.junit.tab"), new JUnitConfigurable<>(getProject()));
        JavaRunConfigurationExtensionManager.getInstance().appendEditors(this, group);
        group.addEditor(ExecutionBundle.message("logs.tab.title"), new LogConfigurationPanel<>());
        return group;
    }

    @Override
    public HybrisTestConfiguration clone() {
        return (HybrisTestConfiguration) super.clone();
    }

    @Override
    public void readExternal(final Element element) throws InvalidDataException {
        super.readExternal(element);
        final Element child = element.getChild(HYBRIS_TAG);

        this.classAnnotationFilter = child == null ? null : NamedEnumUtil.getEnumElementByValue(
            ClassAnnotationTestFilter.class,
            child.getAttributeValue(FILTER_ATTRIBUTE)
        );
    }

    @Override
    public void writeExternal(final Element element) throws WriteExternalException {
        super.writeExternal(element);

        if (classAnnotationFilter != null) {
            final Element child = new Element(HYBRIS_TAG);
            child.setAttribute(FILTER_ATTRIBUTE, NamedEnumUtil.getEnumValueByElement(classAnnotationFilter));
            element.addContent(child);
        }
    }

    @Nullable
    public ClassAnnotationTestFilter getClassAnnotationFilter() {
        return classAnnotationFilter;
    }

    public void setClassAnnotationFilter(@Nullable final ClassAnnotationTestFilter classAnnotationFilter) {
        this.classAnnotationFilter = classAnnotationFilter;
    }

    @Nullable
    public ScopeTestFilter getScopeTestFilter() {
        return scopeTestFilter;
    }

    public void setScopeTestFilter(@Nullable final ScopeTestFilter scopeTestFilter) {
        this.scopeTestFilter = scopeTestFilter;
    }

    @Nullable
    public Module getFilteringModule() {
        return filteringModule;
    }

    public void setFilteringModule(@Nullable final Module module) {
        this.filteringModule = module;
    }

    @Override
    public void checkConfiguration() throws RuntimeConfigurationException {
        super.checkConfiguration();

        if (scopeTestFilter == ScopeTestFilter.MODULE && filteringModule == null) {
            throw new RuntimeConfigurationException(message("hybris.tests.module.is.not.specified.error"));
        }
    }

    @Override
    public SMTRunnerConsoleProperties createTestConsoleProperties(final Executor executor) {
        return new HybrisTestConsoleProperties(this, executor);
    }

    @Override
    public TestObject getState(
        @NotNull final Executor executor,
        @NotNull final ExecutionEnvironment env
    ) throws ExecutionException {
        final TestObject testObject = fromString(getPersistentData().TEST_OBJECT, this, env);
        return testObject != null ? testObject : super.getState(executor, env);
    }

    @Nullable
    private static TestObject fromString(
        @Nullable final String id,
        @NotNull final JUnitConfiguration configuration,
        @NotNull final ExecutionEnvironment environment
    ) {
        if (JUnitConfiguration.TEST_PACKAGE.equals(id)) {
            return new HybrisTestPackage(configuration, environment);
        }
        if (JUnitConfiguration.TEST_PATTERN.equals(id)) {
            return new HybrisTestsPattern(configuration, environment);
        }
        return null;
    }

    private static class MyData extends JUnitConfiguration.Data {

        @Override
        public TestObject getTestObject(@NotNull final JUnitConfiguration configuration) {
            final ExecutionEnvironment environment = ExecutionEnvironmentBuilder.create(
                DefaultRunExecutor.getRunExecutorInstance(),
                configuration
            ).build();

            final TestObject testObject = fromString(TEST_OBJECT, configuration, environment);
            return testObject != null ? testObject : super.getTestObject(configuration);
        }

        @Override
        public MyData clone() {
            return (MyData) super.clone();
        }
    }
}
