/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.Executor;
import com.intellij.execution.RunManager;
import com.intellij.execution.RunnerAndConfigurationSettings;
import com.intellij.execution.executors.DefaultDebugExecutor;
import com.intellij.execution.executors.DefaultRunExecutor;
import com.intellij.execution.impl.RunDialog;
import com.intellij.execution.runners.ExecutionEnvironmentBuilder;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.wm.ToolWindowId;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.HyperlinkAdapter;
import org.jetbrains.annotations.NotNull;

import javax.swing.event.HyperlinkEvent;

/**
 * @author Eugene.Kudelevsky
 */
public class FixNoTestsHyperlinkAdapter extends HyperlinkAdapter {

    private static final Logger LOG = Logger.getInstance(FixNoTestsHyperlinkAdapter.class);

    public static final String HREF_FIX_CLASSPATH = "fix_classpath";
    public static final String HREF_EDIT_CONFIGURATION = "edit_configuration";

    @NotNull
    private final Project project;
    @NotNull
    private final HybrisTestConfiguration configuration;
    private final boolean debug;

    public FixNoTestsHyperlinkAdapter(
        @NotNull final Project project,
        @NotNull final HybrisTestConfiguration configuration,
        final boolean debug
    ) {
        this.project = project;
        this.configuration = configuration;
        this.debug = debug;
    }

    @Override
    protected void hyperlinkActivated(final HyperlinkEvent e) {
        final String description = e.getDescription();

        if (HREF_FIX_CLASSPATH.equals(description)) {
            fixClasspathAndRestart();
        } else if (HREF_EDIT_CONFIGURATION.equals(description)) {
            editConfiguration();
        }
    }

    private void editConfiguration() {
        final String configurationName = configuration.getName();

        if (configurationName == null) {
            LOG.debug("Configuration has no name");
            return;
        }
        final RunnerAndConfigurationSettings configurationSettings =
            RunManager.getInstance(project).findConfigurationByName(configurationName);

        if (configurationSettings == null) {
            LOG.debug("Cannot find configuration " + configurationName);
            return;
        }
        RunDialog.editConfiguration(project, configurationSettings, "Edit Configuration");
    }

    private void fixClasspathAndRestart() {
        final Module allLocalExtModule = ModuleManager.getInstance(project).findModuleByName(
            LocalExtensionsModuleGenerator.LOCAL_EXTENSIONS_MODULE_NAME);

        if (allLocalExtModule == null) {
            return;
        }
        configuration.getConfigurationModule().setModule(allLocalExtModule);
        try {
            final Executor executor = debug ? DefaultDebugExecutor.getDebugExecutorInstance()
                : DefaultRunExecutor.getRunExecutorInstance();
            ExecutionEnvironmentBuilder
                .create(project, executor, configuration)
                .contentToReuse(null).buildAndExecute();

            final String toolWindowId = getToolWindowId(debug);
            final Balloon balloon = ToolWindowManager.getInstance(project).getToolWindowBalloon(toolWindowId);

            if (balloon != null) {
                balloon.hide();
            }
        } catch (ExecutionException e) {
            LOG.error(e);
        }
    }

    @NotNull
    public static String getToolWindowId(final boolean debug) {
        return debug ? ToolWindowId.DEBUG : ToolWindowId.RUN;
    }
}
