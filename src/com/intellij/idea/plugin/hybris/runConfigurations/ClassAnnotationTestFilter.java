/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassOwner;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collections;

import static com.intellij.codeInsight.AnnotationUtil.findAnnotationInHierarchy;
import static com.intellij.idea.plugin.hybris.common.utils.HybrisI18NBundleUtils.message;

/**
 * @author Eugene.Kudelevsky
 */
public enum ClassAnnotationTestFilter {
    INTEGRATION_TESTS(message("hybris.tests.integration.tests.filter.name"), "de.hybris.bootstrap.annotations.IntegrationTest"),
    UNIT_TESTS(message("hybris.tests.unit.tests.filter.name"), "de.hybris.bootstrap.annotations.UnitTest");

    @NotNull
    private final String name;
    @NotNull
    private final String annotationClassQName;

    ClassAnnotationTestFilter(@NotNull final String name, @NotNull final String annotationClassQName) {
        this.name = name;
        this.annotationClassQName = annotationClassQName;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public boolean check(@NotNull final Project project, @NotNull final VirtualFile file) {
        final PsiFile psiFile = PsiManager.getInstance(project).findFile(file);

        return psiFile instanceof PsiClassOwner &&
               Arrays.stream(((PsiClassOwner) psiFile).getClasses()).anyMatch(this::check);
    }

    private boolean check(@NotNull final PsiClass cls) {
        return !annotationClassQName.isEmpty() &&
               findAnnotationInHierarchy(cls, Collections.singleton(annotationClassQName)) != null;
    }
}
