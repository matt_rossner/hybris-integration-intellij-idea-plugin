/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.idea.plugin.hybris.common.HybrisConstants;
import com.intellij.idea.plugin.hybris.project.descriptors.DefaultHybrisProjectDescriptor;
import com.intellij.idea.plugin.hybris.project.descriptors.HybrisModuleDescriptor;
import com.intellij.idea.plugin.hybris.project.descriptors.HybrisModuleDescriptorType;
import com.intellij.idea.plugin.hybris.project.settings.jaxb.localextensions.ExtensionType;
import com.intellij.idea.plugin.hybris.project.settings.jaxb.localextensions.Hybrisconfig;
import com.intellij.idea.plugin.hybris.settings.HybrisProjectSettingsComponent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.ModifiableModuleModel;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.module.StdModuleTypes;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.startup.StartupManager;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileContentsChangedAdapter;
import com.intellij.util.containers.ContainerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Eugene.Kudelevsky
 */
public class LocalExtensionsModuleGenerator extends AbstractProjectComponent {

    private static final Logger LOG = Logger.getInstance(LocalExtensionsModuleGenerator.class);
    public static final String LOCAL_EXTENSIONS_MODULE_NAME = "all_local_extensions";

    @NotNull
    private Set<LocalFileSystem.WatchRequest> watchRequests = Collections.emptySet();
    private VirtualFileContentsChangedAdapter virtualFileListener;

    protected LocalExtensionsModuleGenerator(final Project project) {
        super(project);
    }

    @Override
    public void initComponent() {
        StartupManager.getInstance(myProject).runWhenProjectIsInitialized(this::onProjectInitialized);
    }

    private void onProjectInitialized() {
        final Module configModule = findConfigModule();

        if (configModule == null) {
            LOG.info("The component wasn't initialized: cannot find config module");
            return;
        }
        final ModuleRootManager configModuleRootManager = ModuleRootManager.getInstance(configModule);

        final Set<String> configModuleRootPaths = Arrays
            .stream(configModuleRootManager.getContentRootUrls())
            .map(VfsUtil::urlToPath)
            .collect(Collectors.toSet());

        final LocalFileSystem lfs = LocalFileSystem.getInstance();
        watchRequests = lfs.addRootsToWatch(configModuleRootPaths, false);

        virtualFileListener = new VirtualFileContentsChangedAdapter() {

            @Override
            protected void onFileChange(@NotNull final VirtualFile file) {
                if (file.getName().equals(HybrisConstants.LOCAL_EXTENSIONS_XML) &&
                    configModuleRootPaths.contains(file.getParent().getPath())) {
                    updateModule(file.getPath());
                }
            }

            @Override
            protected void onBeforeFileChange(@NotNull final VirtualFile file) {
            }
        };
        lfs.addVirtualFileListener(virtualFileListener);

        final String localExtensionsFilePath = Arrays
            .stream(configModuleRootManager.getContentRoots())
            .map(root -> root.findChild(HybrisConstants.LOCAL_EXTENSIONS_XML))
            .filter(Objects::nonNull)
            .findAny()
            .map(VirtualFile::getPath)
            .orElse(null);

        if (localExtensionsFilePath != null) {
            updateModule(localExtensionsFilePath);
        }
    }

    @Nullable
    private Module findConfigModule() {
        return Arrays
            .stream(ModuleManager.getInstance(myProject).getModules())
            .filter(module -> HybrisModuleDescriptor.getDescriptorType(module) == HybrisModuleDescriptorType.CONFIG)
            .findAny()
            .orElse(null);
    }

    @Override
    public void disposeComponent() {
        final LocalFileSystem lfs = LocalFileSystem.getInstance();
        lfs.removeWatchedRoots(watchRequests);
        watchRequests = Collections.emptySet();

        if (virtualFileListener != null) {
            lfs.removeVirtualFileListener(virtualFileListener);
            virtualFileListener = null;
        }
    }

    @Nullable
    private static Set<String> unmarshalModuleNames(@NotNull final String localExtensionsFilePath) {
        final Hybrisconfig config = DefaultHybrisProjectDescriptor.unmarshalLocalExtensions(new File(
            localExtensionsFilePath));

        if (config == null) {
            return null;
        }
        return config.getExtensions().getExtension().stream()
                     .map(ExtensionType::getName)
                     .map(StringUtil::toLowerCase)
                     .filter(Objects::nonNull)
                     .collect(Collectors.toSet());
    }

    private void updateModule(@NotNull final String localExtensionsFilePath) {
        if (myProject.isDisposed()) {
            return;
        }
        final Module module = getOrCreateModule();

        if (module == null) {
            return;
        }
        final Set<String> localExtModuleNames = unmarshalModuleNames(localExtensionsFilePath);

        if (localExtModuleNames == null) {
            return;
        }
        final ModuleManager moduleManager = ModuleManager.getInstance(myProject);

        final Set<Module> localExtModules = localExtModuleNames
            .stream()
            .map(moduleManager::findModuleByName)
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());

        final ModuleRootManager moduleRootManager = ModuleRootManager.getInstance(module);
        final Set<Module> existingDependencies = ContainerUtil.immutableSet(moduleRootManager.getDependencies());

        if (!existingDependencies.equals(localExtModules)) {
            final ModifiableRootModel modifiableModel = moduleRootManager.getModifiableModel();
            modifiableModel.clear();
            modifiableModel.inheritSdk();
            localExtModules.forEach(modifiableModel::addModuleOrderEntry);
            ApplicationManager.getApplication().runWriteAction(modifiableModel::commit);
        }
    }

    @Nullable
    private Module getOrCreateModule() {
        final ModuleManager moduleManager = ModuleManager.getInstance(myProject);
        final Module existingModule = moduleManager.findModuleByName(LOCAL_EXTENSIONS_MODULE_NAME);

        if (existingModule != null) {
            return existingModule;
        }
        final Module configModule = findConfigModule();

        if (configModule == null) {
            LOG.info("Cannot find config module");
            return null;
        }
        final String moduleFilePath = getModuleFilePathForLocalExtensionsModule(configModule);
        final ModifiableModuleModel modifiableModel = moduleManager.getModifiableModel();
        modifiableModel.newModule(moduleFilePath, StdModuleTypes.JAVA.getId());
        ApplicationManager.getApplication().runWriteAction(modifiableModel::commit);

        return moduleManager.findModuleByName(LOCAL_EXTENSIONS_MODULE_NAME);
    }

    @NotNull
    private String getModuleFilePathForLocalExtensionsModule(@NotNull final Module configModule) {
        final String suffix = '/' + LOCAL_EXTENSIONS_MODULE_NAME + HybrisConstants.NEW_IDEA_MODULE_FILE_EXTENSION;
        final String ideModulesFilesDirectory = HybrisProjectSettingsComponent.getInstance(
            myProject).getState().getIdeModulesFilesDirectory();

        if (StringUtil.isNotEmpty(ideModulesFilesDirectory)) {
            return ideModulesFilesDirectory + suffix;
        }
        final String configModuleDirPath = new File(configModule.getModuleFilePath()).getParent();
        return FileUtil.toSystemIndependentName(configModuleDirPath) + suffix;
    }
}
