/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.application.options.ModulesComboBox;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.ui.IdeBorderFactory;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBRadioButton;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.ui.RadioButtonEnumModel;
import com.intellij.util.ui.UIUtil;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.Set;

import static com.intellij.idea.plugin.hybris.common.utils.HybrisI18NBundleUtils.message;

/**
 * @author Eugene.Kudelevsky
 */
@SuppressWarnings("ParameterNameDiffersFromOverriddenParameter")
public class HybrisTestSettingsEditor extends SettingsEditor<HybrisTestConfiguration> {

    private static final int LEFT_INDENT = 20;

    private final Box box = Box.createVerticalBox();
    private final Box testKindButtonsBox = Box.createVerticalBox();
    private final JBCheckBox filterByTestKindCheckBox = new JBCheckBox(message("hybris.tests.filter.by.type.label"));
    private final RadioButtonEnumModel<ClassAnnotationTestFilter> annotationTestFilterModel;

    private final Box scopeButtonsBox = Box.createVerticalBox();
    private final JBCheckBox filterTestsByScopeCheckBox = new JBCheckBox(message("hybris.tests.filter.by.scope.label"));
    private final RadioButtonEnumModel<ScopeTestFilter> scopeTestFilterModel;
    private final ModulesComboBox modulesComboBox = new ModulesComboBox();

    public HybrisTestSettingsEditor() {
        filterByTestKindCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        box.add(filterByTestKindCheckBox);

        final ButtonGroup annotationTestFilterButtonGroup = new ButtonGroup();

        for (ClassAnnotationTestFilter filter : ClassAnnotationTestFilter.values()) {
            final JBRadioButton button = new JBRadioButton(filter.getName());
            annotationTestFilterButtonGroup.add(button);
            button.setAlignmentX(Component.LEFT_ALIGNMENT);
            testKindButtonsBox.add(button);
        }
        annotationTestFilterModel = RadioButtonEnumModel.bindEnum(
            ClassAnnotationTestFilter.class,
            annotationTestFilterButtonGroup
        );
        testKindButtonsBox.setBorder(IdeBorderFactory.createEmptyBorder(0, LEFT_INDENT, 0, 0));
        testKindButtonsBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        box.add(testKindButtonsBox);

        filterByTestKindCheckBox.addActionListener(e -> {
            UIUtil.setEnabled(testKindButtonsBox, filterByTestKindCheckBox.isSelected(), true);
        });

        box.add(Box.createVerticalStrut(10));
        filterTestsByScopeCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        box.add(filterTestsByScopeCheckBox);

        final ButtonGroup scopeTestFilterButtonGroup = new ButtonGroup();

        for (ScopeTestFilter filter : ScopeTestFilter.values()) {

            if (filter == ScopeTestFilter.MODULE) {
                final JBRadioButton button = new JBRadioButton(filter.getName() + ':');
                scopeTestFilterButtonGroup.add(button);
                final Box moduleBox = Box.createHorizontalBox();
                moduleBox.add(button);
                moduleBox.add(modulesComboBox);
                moduleBox.add(Box.createHorizontalGlue());
                moduleBox.setAlignmentX(Component.LEFT_ALIGNMENT);
                moduleBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, modulesComboBox.getPreferredSize().height));
                scopeButtonsBox.add(moduleBox);
            } else {
                final JBRadioButton button = new JBRadioButton(filter.getName());
                scopeTestFilterButtonGroup.add(button);
                button.setAlignmentX(Component.LEFT_ALIGNMENT);
                scopeButtonsBox.add(button);
            }
        }
        scopeTestFilterModel = RadioButtonEnumModel.bindEnum(
            ScopeTestFilter.class,
            scopeTestFilterButtonGroup
        );
        scopeTestFilterModel.addActionListener(e -> modulesComboBox.setEnabled(
            scopeTestFilterModel.getSelected() == ScopeTestFilter.MODULE));
        scopeButtonsBox.setBorder(IdeBorderFactory.createEmptyBorder(0, LEFT_INDENT, 0, 0));
        scopeButtonsBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        box.add(scopeButtonsBox);

        filterTestsByScopeCheckBox.addActionListener(e -> {
            UIUtil.setEnabled(scopeButtonsBox, filterTestsByScopeCheckBox.isSelected(), true);
        });

        box.add(Box.createVerticalGlue());
    }

    @Override
    protected void resetEditorFrom(@NotNull final HybrisTestConfiguration configuration) {
        final Module localExtensionsModule = ModuleManager.getInstance(configuration.getProject()).
            findModuleByName(LocalExtensionsModuleGenerator.LOCAL_EXTENSIONS_MODULE_NAME);

        if (localExtensionsModule != null) {
            final Set<Module> modules = ContainerUtil.newHashSet();

            ModuleRootManager
                .getInstance(localExtensionsModule)
                .orderEntries()
                .recursively()
                .forEachModule(modules::add);

            modulesComboBox.setModules(modules);
        } else {
            modulesComboBox.setModules(Collections.emptyList());
        }
        final ClassAnnotationTestFilter annotationFilter = configuration.getClassAnnotationFilter();
        filterByTestKindCheckBox.setSelected(annotationFilter != null);
        UIUtil.setEnabled(testKindButtonsBox, annotationFilter != null, true);
        annotationTestFilterModel.setSelected(annotationFilter != null
                                                  ? annotationFilter
                                                  : ClassAnnotationTestFilter.values()[0]);

        final ScopeTestFilter scopeFilter = configuration.getScopeTestFilter();
        filterTestsByScopeCheckBox.setSelected(scopeFilter != null);
        UIUtil.setEnabled(scopeButtonsBox, scopeFilter != null, true);
        scopeTestFilterModel.setSelected(scopeFilter != null ? scopeFilter : ScopeTestFilter.values()[0]);

        modulesComboBox.setEnabled(scopeTestFilterModel.getSelected() == ScopeTestFilter.MODULE);
        modulesComboBox.setSelectedModule(configuration.getFilteringModule());
    }

    @Override
    protected void applyEditorTo(@NotNull final HybrisTestConfiguration configuration) throws ConfigurationException {

        configuration.setClassAnnotationFilter(
            filterByTestKindCheckBox.isSelected()
                ? annotationTestFilterModel.getSelected()
                : null);

        configuration.setScopeTestFilter(
            filterTestsByScopeCheckBox.isSelected()
                ? scopeTestFilterModel.getSelected()
                : null);

        configuration.setFilteringModule(modulesComboBox.getSelectedModule());
    }

    @NotNull
    @Override
    protected JComponent createEditor() {
        return box;
    }
}
