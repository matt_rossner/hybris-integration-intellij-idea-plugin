/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.runConfigurations;

import com.intellij.execution.Executor;
import com.intellij.execution.junit.JUnitConfiguration;
import com.intellij.execution.junit2.ui.properties.JUnitConsoleProperties;
import com.intellij.idea.plugin.hybris.common.HybrisConstants;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.wm.ToolWindowManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.intellij.idea.plugin.hybris.runConfigurations.FixNoTestsHyperlinkAdapter.HREF_EDIT_CONFIGURATION;
import static com.intellij.idea.plugin.hybris.runConfigurations.FixNoTestsHyperlinkAdapter.HREF_FIX_CLASSPATH;
import static com.intellij.idea.plugin.hybris.runConfigurations.LocalExtensionsModuleGenerator.LOCAL_EXTENSIONS_MODULE_NAME;

/**
 * @author Eugene.Kudelevsky
 */
public class HybrisTestConsoleProperties extends JUnitConsoleProperties {

    public HybrisTestConsoleProperties(@NotNull final JUnitConfiguration configuration, final Executor executor) {
        super(configuration, executor);
    }

    @Override
    public boolean fixEmptySuite() {
        //noinspection CastToConcreteClass
        final HybrisTestConfiguration configuration = (HybrisTestConfiguration) getConfiguration();
        final String message = buildNoTestsMessage(configuration);

        if (message == null) {
            return false;
        }
        final boolean debug = isDebug();
        final String toolWindowId = FixNoTestsHyperlinkAdapter.getToolWindowId(debug);
        final Project project = configuration.getProject();
        final FixNoTestsHyperlinkAdapter adapter = new FixNoTestsHyperlinkAdapter(project, configuration, debug);
        ToolWindowManager.getInstance(project).notifyByBalloon(
            toolWindowId, MessageType.WARNING, message, null, adapter);
        return true;
    }

    @NotNull
    private static String getTestsStr(@Nullable final ClassAnnotationTestFilter filter) {
        if (filter != null) {
            switch (filter) {
                case INTEGRATION_TESTS:
                    return "Integration tests";
                case UNIT_TESTS:
                    return "Unit tests";
            }
        }
        return "Tests";
    }

    @Nullable
    private String buildNoTestsMessage(@NotNull final HybrisTestConfiguration configuration) {
        final Module classpathModule = configuration.getConfigurationModule().getModule();

        if (classpathModule == null) {
            return null;
        }
        final String testsStr = getTestsStr(configuration.getClassAnnotationFilter());
        final String classpathModuleName = classpathModule.getName();

        if (!LOCAL_EXTENSIONS_MODULE_NAME.equals(classpathModuleName)) {
            return testsStr + " were not found in the classpath of module \"" + classpathModuleName +
                   "\".\nTry to use module <a href=\"" + HREF_FIX_CLASSPATH + "\">" +
                   LOCAL_EXTENSIONS_MODULE_NAME + "</a> for classpath instead.";
        }
        final ScopeTestFilter scopeTestFilter = configuration.getScopeTestFilter();

        if (scopeTestFilter == null) {
            return testsStr + " were not found in the classpath based on \"" +
                   HybrisConstants.LOCAL_EXTENSIONS_XML + '"';
        }
        final String editHref = "<a href=\"" + HREF_EDIT_CONFIGURATION + "\">Edit Configuration</a>";

        if (scopeTestFilter == ScopeTestFilter.ALL_CUSTOM_MODULES) {
            return testsStr + " were not found in custom modules.\n" + editHref;
        }
        final Module filteringModule = configuration.getFilteringModule();

        if (filteringModule != null) {
            return testsStr + " were not found in module \"" + filteringModule.getName() + "\".\n" + editHref;
        }
        return null;
    }
}
