/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.statistics;

import com.intellij.execution.ExecutionListener;
import com.intellij.execution.ExecutionManager;
import com.intellij.execution.RunnerAndConfigurationSettings;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.idea.plugin.hybris.runConfigurations.ClassAnnotationTestFilter;
import com.intellij.idea.plugin.hybris.runConfigurations.HybrisTestConfiguration;
import com.intellij.idea.plugin.hybris.statistics.StatsCollector.ACTIONS;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.IdeActions;
import com.intellij.openapi.actionSystem.ex.AnActionListener;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.util.messages.MessageBusConnection;
import org.jetbrains.annotations.NotNull;

/**
 * @author Eugene.Kudelevsky
 */
public class StatsCollectingInitializer implements ApplicationComponent {

    @NotNull
    private final ActionManager actionManager;

    public StatsCollectingInitializer(

        @NotNull final ActionManager actionManager
    ) {
        this.actionManager = actionManager;
    }

    @Override
    public void initComponent() {
        actionManager.addAnActionListener(new AnActionListener.Adapter() {

            @Override
            public void afterActionPerformed(
                final AnAction action, final DataContext dataContext, final AnActionEvent event
            ) {
                onActionPerformed(action);
            }
        });
        final MessageBusConnection connection = ApplicationManager.getApplication().getMessageBus().connect();

        connection.subscribe(ExecutionManager.EXECUTION_TOPIC, new ExecutionListener() {

            @Override
            public void processStarted(
                @NotNull final String executorId,
                @NotNull final ExecutionEnvironment env,
                @NotNull final ProcessHandler handler
            ) {
                onExecutionProcessStarted(executorId, env);
            }
        });
    }

    private void onActionPerformed(final AnAction action) {
        @NotNull final AnAction buildProjectAction = actionManager.getAction("CompileDirty");
        @NotNull final AnAction rebuildProjectAction = actionManager.getAction(IdeActions.ACTION_COMPILE_PROJECT);
        final StatsCollector statsCollector = StatsCollector.getInstance();

        if (buildProjectAction.equals(action)) {
            statsCollector.collectStat(ACTIONS.BUILD_PROJECT, "rebuild:false");
        } else if (rebuildProjectAction.equals(action)) {
            statsCollector.collectStat(ACTIONS.BUILD_PROJECT, "rebuild:true");
        }
    }

    private static void onExecutionProcessStarted(
        final @NotNull String executorId,
        final @NotNull ExecutionEnvironment env
    ) {
        final RunnerAndConfigurationSettings settings = env.getRunnerAndConfigurationSettings();
        final RunConfiguration configuration = settings == null ? null : settings.getConfiguration();

        if (configuration instanceof HybrisTestConfiguration) {
            //noinspection CastToConcreteClass
            final ClassAnnotationTestFilter annotationFilter =
                ((HybrisTestConfiguration) configuration).getClassAnnotationFilter();

            StatsCollector.getInstance().collectStat(
                ACTIONS.RUN_TESTS,
                "executor:" + executorId + ",annotation-filter:" + annotationFilter
            );
        }
    }

}
